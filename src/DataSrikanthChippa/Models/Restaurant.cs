﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataSrikanthChippa.Models
{
    public class Restaurant
    {
        [ScaffoldColumn(false)]

        public int RestaurantID { get; set; }
        public string Restaurantname { get; set; }
        public string ManagerName { get; set; }
        [ScaffoldColumn(false)]
        public int locationId { get; set; }
        
    }
}
